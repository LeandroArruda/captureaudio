var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition;
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList;
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent;

var phrasePara = document.querySelector('.phrase');
var resultPara = document.querySelector('.result');
var diagnosticPara = document.querySelector('.output');
var body = document.querySelector('body')
var divCont = document.querySelector('.contador')
var contador = JSON.parse(localStorage.getItem('contador')) || 0
divCont.innerHTML = contador
var test = JSON.parse(localStorage.getItem('test')) || 0
var test1 = JSON.parse(localStorage.getItem('test1')) || 0
var phraseAux = Object.keys(phrases)[randomPhrase()]
let array = []


function shuffle(o) {
  for(let j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
};

function popularArray() {
  for(let i = 0; i < Object.keys(phrases).length; i++) {
    array.push(i)
  }
  array = JSON.parse(localStorage.getItem('array')) || shuffle(array)
  localStorage.setItem('array',JSON.stringify(array))
}

popularArray()


function randomPhrase() {
  var number = Math.floor(Math.random() * Object.keys(phrases).length);
  return number;
}

function leitura () {
  if(test === Object.keys(phrases).length -1) {
    localStorage.setItem('test',0)
    localStorage.setItem('test1',0)
    localStorage.setItem('contador',0)
    localStorage.removeItem('array')
    test = 0
    test1 = 0
    contador = 0
    array = []
    popularArray()
  }
  if(test === test1 ){
    var phrase = Object.keys(phrases)[array[test]];
    test++
    localStorage.setItem('test',JSON.stringify(test))
    array.push(phrase)
  }else{
    var phrase = phraseAux
  }

  phraseAux = phrase
    phrase = phrase.toUpperCase();
    phrasePara.textContent = phrases[phrase];
    body.style.background = 'rgba(124, 165, 161)';
    diagnosticPara.textContent = '...esperando';
  
    var grammar = '#JSGF V1.0; grammar phrase; public <phrase> = ' + phrase +';';
    var recognition = new SpeechRecognition();
    var speechRecognitionList = new SpeechGrammarList();
    speechRecognitionList.addFromString(grammar, 1);
    recognition.grammars = speechRecognitionList;
    recognition.lang = 'pt-BR';
    recognition.interimResults = false;
    recognition.maxAlternatives = 1;
  
    recognition.start();
  
    recognition.onresult = function(event) {
      var speechResult = event.results[0][0].transcript.toUpperCase();
      diagnosticPara.textContent = 'Você falou ' + speechResult + '?';
        if(speechResult === phrase) {
          body.style.background = 'lime';
          contador++
          localStorage.setItem('contador',JSON.stringify(contador))
          divCont.innerHTML = contador
          test1++
          localStorage.setItem('test1',JSON.stringify(test1))
        } else {
          body.style.background = 'red';
        }
        console.log('Confidence: ' + event.results[0][0].confidence);
    }
  
    recognition.onspeechend = function() {
      recognition.stop();
    }
  
    recognition.onerror = function(event) {
      diagnosticPara.textContent = 'hmm... Não consegui ouvir.'
    }
    
    recognition.onaudiostart = function(event) {
        console.log('SpeechRecognition.onaudiostart');
    }
    
    recognition.onaudioend = function(event) {
        console.log('SpeechRecognition.onaudioend');
    }
    
    recognition.onend = function(event) {
        console.log('SpeechRecognition.onend');
    }
    
    recognition.onnomatch = function(event) {
      console.log('SpeechRecognition.onnomatch');
    }
    
    recognition.onsoundstart = function(event) {
        console.log('SpeechRecognition.onsoundstart');
    }
    
    recognition.onsoundend = function(event) {
        console.log('SpeechRecognition.onsoundend');
    }
    
    recognition.onspeechstart = function (event) {
        console.log('SpeechRecognition.onspeechstart');
    }
    recognition.onstart = function(event) {
       console.log('SpeechRecognition.onstart');
    }

}

document.addEventListener('keydown', leitura)

document.addEventListener('click',leitura)


